#!/bin/sh

if [ -e "./variables" ]; then
  echo "Sourcing variables from previous stage"
  source ./variables
else
  echo "No variables available. I'm not sourcing anything"
fi

BUILD_IT="false"

echo "Trying to build ${DOCKERFILE}, tagged as ${CI_REGISTRY_IMAGE}:${IMAGE_TAG} (building args: ${BUILD_ARGS})"
echo "Checking build against last commit ${LAST_COMMIT}"

# Tests to determine whether to build
if [ "`echo ${REBUILD_ALL} | tr [:upper:] [:lower:]`" = "true" ]; then
  echo "Building ${DOCKERFILE} (${IMAGE_TAG}) from rebuild all request"
  BUILD_IT="true"
elif [ "${REBUILD_STAGE}" = "${CI_JOB_STAGE}" ]; then
  echo "Building ${DOCKERFILE} (${IMAGE_TAG}) from rebuild image stage request"
  BUILD_IT="true"
elif [ "${REBUILD_JOB}" = "${CI_JOB_NAME}" ]; then
  echo "Building ${DOCKERFILE} (${IMAGE_TAG}) from rebuild image job request"
  BUILD_IT="true"
elif git diff ${LAST_COMMIT} HEAD --name-only | grep "${DOCKERFILE}.Dockerfile"; then
  echo "Building ${DOCKERFILE} (${IMAGE_TAG}) since it changed from commit ${LAST_COMMIT}"
  BUILD_IT="true"
else
  echo "${DOCKERFILE} up to date, not building it"
fi

# Actual build stage
if [ "`echo ${BUILD_IT} | tr [:upper:] [:lower:]`" = "true" ]; then
  docker build --pull ${BUILD_ARGS} -t "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}" -f "${DOCKERFILE}.Dockerfile" .
  docker push "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}"
  echo "Marking all future stages for rebuild"
  echo "REBUILD_ALL=true" >> variables
  if [ "${LATEST}" = "${IMAGE_TAG}" ]; then
    echo "Pushing ${IMAGE_TAG} as latest too"
    docker tag "${CI_REGISTRY_IMAGE}:${IMAGE_TAG}" "${CI_REGISTRY_IMAGE}:latest"
    docker push "${CI_REGISTRY_IMAGE}:latest"
  fi
fi