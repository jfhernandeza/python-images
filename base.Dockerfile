ARG BASE_IMAGE=tensorflow/tensorflow:latest-py3
FROM ${BASE_IMAGE}
MAINTAINER mipl


RUN pip install \
        imutils \
        keras \
        numpy \
        scikit-image \
        scikit-learn \
       	tflearn \
        tensorflow-probability

CMD ["/bin/bash"]
