# Docker for Research

A docker version with `python3`, `tensorflow:lastest` and `opencv 3` for compile and test everything Tensorflow-OpenCV based application.

This project generates the following flavors (both for CPU and GPU)
- `base`: a minimal expansion on the `tensorflow:latest-py3`. 
- `tools`: constructs on top of `base` and adds a set of most common python libraries.
- `opencv`: constructs on top of `tools` and includes OpenCV.

The pipeline constructs the latest version and the 1.12.0.  


## Execute scripts

The following script will execute the scripts `/script/to/run; /or/set/of/them` as if they were run on your machine.

In order to see what is going inside the docker we need to sync the displays with:

`--env DISPLAY=$DISPLAY --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"` if you use **Linux based systems**.

`-e DISPLAY=${HOST_IP}:0` if you use **OSX systems**.


Also, we will share the source code directories `-v=$(pwd)/..:$(pwd)/..` (this assumes you are doing an out-of-source build and you have a parent directory with the `build` and `src` as children). And we set the current directory as our working directory `-w=$(pwd)`.

CPU version:

    docker run -it --rm \
      --env DISPLAY=$DISPLAY --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
      -v=$(pwd)/..:$(pwd)/.. -w=$(pwd) \
      mipl/tf:latest \
      /script/to/run; /or/set/of/them

GPU version:

    nvidia-docker run -it --rm \
      --env DISPLAY=$DISPLAY --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
      -v=$(pwd)/..:$(pwd)/.. -w=$(pwd) \
      mipl/tf:latest-gpu \
      /script/to/run; /or/set/of/them


## Build the image locally

You can build the image locally by following the same logic as the `.gitlab-ci.yml`.

For the base image you will execute
```bash
docker build --pull --build-arg BASE_IMAGE=tensorflow/tensorflow:latest-py3 -t "tf" -f "base.Dockerfile" .
```

Similarly, you can build the other images, and use them as a chain.  The dependency is `base`->`tools`->`opencv`. 