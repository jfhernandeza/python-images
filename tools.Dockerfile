ARG BASE_IMAGE
FROM ${BASE_IMAGE}
MAINTAINER mipl


RUN apt-get -y update -qq && \
    apt-get -y install \
                       # Required
                       build-essential \
                       cmake \
                       git \
                       pkg-config \
    && \
    pip install \
        dlib \
        h5py \
        jupyterlab \
        mnist \
        slackclient \
        tqdm \
        urllib3 \
        pipreqs \
    &&\
    \
    apt-get autoclean autoremove &&\
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*
